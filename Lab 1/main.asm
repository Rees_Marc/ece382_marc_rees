;-------------------------------------------------------------------------------
; MSP430 Assembler Code Template for use with TI Code Composer Studio
;
;
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------

            		.text                           					; Assemble into program memory.
;Demo_1:		.byte		0x11, 0x11, 0x11, 0x11, 0x11, 0x44, 0x22, 0x22, 0x22, 0x11, 0xCC, 0x55																														; Calculator instruction sets
;Demo_2:		.byte		0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0xDD, 0x44, 0x08, 0x22, 0x09, 0x44, 0xFF, 0x22, 0xFD, 0x55
;Demo_3:		.byte		0x22, 0x11, 0x22, 0x22, 0x33, 0x33, 0x08, 0x44, 0x08, 0x22, 0x09, 0x44, 0xff, 0x11, 0xff, 0x44, 0xcc, 0x33, 0x02, 0x33, 0x00, 0x44, 0x33, 0x33, 0x08, 0x55
Demo_4:		.byte		0xFF, 0x33, 0xFF, 0x44, 0xEE, 0x22, 0xFF, 0x33, 0xFF, 0x55

myProgram:	.equ	0x0200

ADD_OP:		.equ	0x11
SUB_OP:		.equ	0x22
MUL_OP:		.equ	0x33
CLR_OP:		.equ	0x44
END_OP:		.equ	0x55

					.data
myResults:	.space	20
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------
				mov		#myProgram, r4			; Starts the memory at 0x0200 in RAM and sets it into r4
				mov		#myResults, 0(r4)		; Reserves bytes in RAM at 0x0200
;				mov		#Demo_1, r5
;				mov		#Demo_2, r5
;				mov		#Demo_3, r5
				mov		#Demo_4, r5				; Places Demo into r5
Clear:		clr			r6								; Clear register for first operand (first byte)
				clr			r7								; Clear register for operation (second byte)
				clr			r8								; Clear register for second operand (third byte)
				clr			r9								; Clear register for counter for multiplications operation
				clr			r10							; Clear register for addition inside of multiplication operation

				mov.b	@r5, 0(r4)					; Places the first value in Demo and puts it in myResults
				inc		r5								; Increments Demo
				mov.b	@r4, r6						; Places value in myProgram into r6 (This will be the first operand)

Main:		mov.b	@r5, r7						; Places value in Demo into r7 (This will be the operations byte)

				cmp.b	#END_OP, r7				; Checks to see if value in r7 is equal to END_OP
				jz			End							; If it equals it will jump to subroutine End
				cmp.b	#CLR_OP, r7				; Checks to see if value in r7 is equal to CLR_OP
				jz			Clr							; If it equals it will jump to subroutine Clr

				inc		r5								; Increments Demo
				mov.b	@r5, r8						; If it does not equal END_OP or CLR_OP it will place value in Demo into r8 (This will be the second operand)
				inc		r5								; Increments Demo

				cmp.b	#ADD_OP, r7				; Checks to see if value in r7 is equal to ADD_OP
				jz			Add							; If it equals it will jump to subroutine Add
				cmp.b	#SUB_OP, r7				; Checks to see if value in r7 is equal to SUB_OP
				jz			Sub							; If it equals it will jump to subroutine Sub
				cmp.b	#MUL_OP, r7				; Checks to see if value in r7 is equal to MUL_OP
				jz			Mul							; If it equals it will jump to subroutine Mul

Add:			add		r6, r8							; Adds r6 and r8 and the resultant is placed into r8
				cmp		#0xFF, r8					; Compares r8 to see if the addition has an overflow
				jc			Add_2						; If Carry bit is set to 1 then jump to subroutine Add_2
				jmp		Add_F						; If not jump to subroutine Add_F

Add_2:		mov.b	#0xFF, r8					; Because C = 1, we need to place 0xFF into r8
				jmp		Add_F						; Jumps to subroutine Add_F

Add_F:		mov.b	r8, 0(r4)						; Places value in r8 into myResults
				mov.b	@r4, r6						; Places value in myProgram into r6
				inc		r4								; Increments myProgram
				jmp		Main							; Jumps back to Main

Sub:			sub		r8, r6							; Subtracts r8 and r6 and places resultant into r6
				jn			Neg							; If Negative bit is set to 1 then jumps to subroutine Neg
				jmp		Sub_F						; If not jump to subroutine Sub_F

Neg:			clr.b		r6								; Becuase N = 1, we need to clear the bit of what is in r6

Sub_F:		mov.b	r6, 0(r4)						; Places value in r6 into myResults
				inc		r4								; Increments myProgram
				jmp		Main							; Jumps back to Main

Mul:			mov.b	r6, r9							; Moves value in r6 into r9 to become a counter
				jmp		Mul_2						; Jumps to subroutine Mul_2

Mul_2:		cmp.b	#0x00, r8					; Compares r8 to make sure a 0x00 is not being multiplied to r6
				jz			Mul_Z						; If it equals then jump to subroutine Mul_Z
				cmp.b	#0x00, r6					; Compares r6 to make sure a 0x00 is not being multiplied to r8
				jz			Mul_Z						; If it equals then jump to subroutine Mul_Z
				add		r8, r10						; Add r8 and r10 and stores the resultant into r10
				dec		r9								; Decrements r9 until it reaches zero
				cmp.b	#0x00, r9					; Compares to see if r9 has reached zero
				jnz		Mul_2						; If r9 has not reached zero it will jump back to subroutine Mul_2
				cmp		#0x00FF, r10				; Compares to see if resultant would be an overflow
				mov.b	r10, r6						; Places value from the resultant into r6
				jc		Mul_O							; If Carry bit is set to 1 then jump to subroutine Mul_O
				jmp		Mul_F						; If not it will jump to subroutine Mul_F

Mul_O:		mov.b	#0xFF, r6					; Because C = 1, we need to place 0xFF into r6
				jmp		Mul_F						; Jump to subroutine Mul_F when done

Mul_Z:		mov.b	#0x00, r6					; If r6 or r8 are 0x00, 0x00 will be placed into r6
				jmp		Mul_F						; Jumps to subroutine Mul_F when done

Mul_F:		mov.b	r6, 0(r4)						; Places value in r6 into myResults
				inc 		r4								; Increments myProgram
				jmp 		Main							; Jumps back to Main

Clr:			clr.b		0(r4)							; Clears bit in myResults
				inc		r4								; Increments myProgram
				inc		r5								; Increments Demo
				jmp		Clear							; Jumps back up to clear to restart the program

End: 			jmp		End							; Jumps back to Ene (CPU trap)

                                            

;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
