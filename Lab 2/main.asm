;**********************************************************************
;
; COPYRIGHT 2016 United States Air Force Academy All rights reserved.
;
; United States Air Force Academy     __  _______ ___    _________
; Dept of Electrical &               / / / / ___//   |  / ____/   |
; Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
; 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
; USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
;
; ----------------------------------------------------------------------
;
;   FILENAME      : Lab 2
;   AUTHOR(S)     : Marc Rees
;   DATE         	 : 9/26/2018
;   COURSE		 : ECE 382
;
;   Lab 2
;
;   DESCRIPTION   : This code simply provides a template for all
;                   C assignments to use.
;                     - Be sure to include your *Documentation*
;                       Statement below!
;
;   DOCUMENTATION : Cadet Oliver helped me by explaining how to set up SER,
;	SRCLK, RCLK.
;
; Academic Integrity Statement: I certify that, while others may have
; assisted me in brain storming, debugging and validating this program,
; the program itself is my own work. I understand that submitting code
; which is the work of other individuals is a violation of the honor
; code.  I also understand that if I knowingly give my original work to
; another individual is also a violation of the honor code.
;
;**********************************************************************
;-------------------------------------------------------------------------------
; MSP430 Assembler Code Template for use with TI Code Composer Studio
;
;
;-------------------------------------------------------------------------------
            		.cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            		.def    RESET                  		 ; Export program entry-point to
                                           						 ; make it known to linker.
;-------------------------------------------------------------------------------
            		.text                           			; Assemble into program memory.
            		.retain                         			; Override ELF conditional linking
                                           						; and retain current section.
            		.retainrefs                     			; And retain any sections that have
                                           						 ; references to current section.

;-------------------------------------------------------------------------------
RESET     		mov.w   #__STACK_END,SP        				 ; Initialize stackpointer
StopWDT		mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------

;-------------------------------------------------------------------------------
; Initializes Pin set up
;-------------------------------------------------------------------------------

					; Button setup
					bic.b		#BIT3,	&P1SEL   		; GPIO
           			bic.b		#BIT3,	&P1SEL2		; GPIO
            		bic.b		#BIT3,	&P1DIR  		; Pin set to input
           			bis.b		#BIT3,	&P1REN   		; Resistor enabled
           			bis.b		#BIT3,	&P1OUT  		; Pull-up

           			; SRCLK setup
           			bic.b		#BIT4, 	&P1SEL			; GPIO,
           			bic.b 	#BIT4, 	&P1SEL2		; GPIO
           			bis.b 	#BIT4, 	&P1DIR			; Pin set to output
           			bic.b 	#BIT4,	&P1OUT		; SRCLK set to 0

					; SER setup
					bic.b 	#BIT5,	&P1SEL			; GPIO
					bic.b 	#BIT5,	&P1SEL2		; GPIO
					bis.b 	#BIT5, 	&P1DIR			; Pin set to ouput
					bic.b 	#BIT5, 	&P1OUT		; SER set to 0

					; RCLK setup
					bic.b 	#BIT0, 	&P2SEL			; GPIO
					bic.b 	#BIT0, 	&P2SEL2		; GPIO
					bis.b 	#BIT0,	&P2DIR			; Pin set to output
					bic.b 	#BIT0, 	&P2OUT		; RCLK set to 0

					clr		r4
					mov.b 	#1, 	r4
					clr		r5

;-------------------------------------------------------------------------------
; Initializes Button presses
;-------------------------------------------------------------------------------

Unpressed_Button:

					bit.b		#BIT3, 	&P1IN			; Test to see if pin input has changed
					jnz		Unpressed_Button		; Jumps back to beginning  if input is not equal
					call 		#Slow_button			; Calls subroutine Slow_button

Pressed_Button:

					bit.b		#BIT3, 	&P1IN			; Test to see if pin input has changed
					jz			Pressed_Button			; Jumps back to beginning of Pressed_Button if input is equal
					call 		#Slow_button			; Calls subroutine Slow_button

					call		#Cycle_light				; Calls Software_delay to stop arcing
					jmp		Unpressed_Button		; Jumps back to Unpressed_Button when button is released

;-------------------------------------------------------------------------------
; Sets up delay
;-------------------------------------------------------------------------------

Slow_button:
					mov.w	#0x00BB, 	r5					; Delays for 2 Clk Cycles
					ret

;-------------------------------------------------------------------------------
; Starts cycle of lights
;-------------------------------------------------------------------------------

Cycle_light:
					cmp		#1, r4
					jz			Q_a

					cmp		#2, r4
					jz			Q_b

					cmp		#3, r4
					jz			Q_c

					cmp		#4, r4
					jz			Q_d

Q_a:
					bic.b		#BIT5, 	&P1OUT		; Sets SER pin to 0
					bis.b		#BIT4, 	&P1OUT		; Sets SRCLK to 1
					bic.b		#BIT4,	&P1OUT		; Sets SRCLK to 0

					bic.b		#BIT5, 	&P1OUT		; Sets SER pin to 0
					bis.b		#BIT4, 	&P1OUT		; Sets SRCLK to 1
					bic.b		#BIT4,	&P1OUT		; Sets SRCLK to 0

					bic.b		#BIT5, 	&P1OUT		; Sets SER pin to 0
					bis.b		#BIT4, 	&P1OUT		; Sets SRCLK to 1
					bic.b		#BIT4,	&P1OUT		; Sets SRCLK to 0

					bis.b		#BIT5, 	&P1OUT		; Sets SER pin to 1
					bis.b		#BIT4, 	&P1OUT		; Sets SRCLK to 1
					bic.b		#BIT4,	&P1OUT		; Sets SRCLK to 0

					inc.b		r4

					bis.b		#BIT0,	&P2OUT		; Sets RCLK to 1
					call		#Clear
					ret

Q_b:
					bic.b		#BIT5, 	&P1OUT		; Sets SER pin to 0
					bis.b		#BIT4, 	&P1OUT		; Sets SRCLK to 1
					bic.b		#BIT4,	&P1OUT		; Sets SRCLK to 0

					bic.b		#BIT5, 	&P1OUT		; Sets SER pin to 0
					bis.b		#BIT4, 	&P1OUT		; Sets SRCLK to 1
					bic.b		#BIT4,	&P1OUT		; Sets SRCLK to 0

					bis.b		#BIT5, 	&P1OUT		; Sets SER pin to 1
					bis.b		#BIT4, 	&P1OUT		; Sets SRCLK to 1
					bic.b		#BIT4,	&P1OUT		; Sets SRCLK to 0

					bic.b		#BIT5, 	&P1OUT		; Sets SER pin to 0
					bis.b		#BIT4, 	&P1OUT		; Sets SRCLK to 1
					bic.b		#BIT4,	&P1OUT		; Sets SRCLK to 0

					inc.b		r4

					bis.b		#BIT0,	&P2OUT		; Sets RCLK to 1
					call		#Clear
					ret

Q_c:
					bic.b		#BIT5, 	&P1OUT		; Sets SER pin to 0
					bis.b		#BIT4, 	&P1OUT		; Sets SRCLK to 1
					bic.b		#BIT4,	&P1OUT		; Sets SRCLK to 0

					bis.b		#BIT5, 	&P1OUT		; Sets SER pin to 1
					bis.b		#BIT4, 	&P1OUT		; Sets SRCLK to 1
					bic.b		#BIT4,	&P1OUT		; Sets SRCLK to 0

					bic.b		#BIT5, 	&P1OUT		; Sets SER pin to 0
					bis.b		#BIT4, 	&P1OUT		; Sets SRCLK to 1
					bic.b		#BIT4,	&P1OUT		; Sets SRCLK to 0

					bic.b		#BIT5, 	&P1OUT		; Sets SER pin to 0
					bis.b		#BIT4, 	&P1OUT		; Sets SRCLK to 1
					bic.b		#BIT4,	&P1OUT		; Sets SRCLK to 0

					inc.b		r4

					bis.b		#BIT0,	&P2OUT		; Sets RCLK to 1
					call		#Clear
					ret

Q_d:
					bis.b		#BIT5, 	&P1OUT		; Sets SER pin to 1
					bis.b		#BIT4, 	&P1OUT		; Sets SRCLK to 1
					bic.b		#BIT4,	&P1OUT		; Sets SRCLK to 0

					bic.b		#BIT5, 	&P1OUT		; Sets SER pin to 0
					bis.b		#BIT4, 	&P1OUT		; Sets SRCLK to 1
					bic.b		#BIT4,	&P1OUT		; Sets SRCLK to 0

					bic.b		#BIT5, 	&P1OUT		; Sets SER pin to 0
					bis.b		#BIT4, 	&P1OUT		; Sets SRCLK to 1
					bic.b		#BIT4,	&P1OUT		; Sets SRCLK to 0

					bic.b		#BIT5, 	&P1OUT		; Sets SER pin to 0
					bis.b		#BIT4, 	&P1OUT		; Sets SRCLK to 1
					bic.b		#BIT4,	&P1OUT		; Sets SRCLK to 0

					mov.b	#1	, r4

					bis.b		#BIT0,	&P2OUT		; Sets RCLK to 1
					call		#Clear
					ret

;-------------------------------------------------------------------------------
; Clear pins
;-------------------------------------------------------------------------------

Clear:
					bic.b		#BIT0,	&P2OUT		; Sets RCLK to 0
					bic.b		#BIT5,	&P1OUT		; Sets SER to 0
					bic.b		#BIT4,	&P1OUT		; Sets SRCLK to 0
					ret

;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            		.global __STACK_END
            		.sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            		.sect   ".reset"                ; MSP430 RESET Vector
            		.short  RESET
            
