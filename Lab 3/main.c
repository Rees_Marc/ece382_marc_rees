;**********************************************************************
;
; COPYRIGHT 2016 United States Air Force Academy All rights reserved.
;
; United States Air Force Academy     __  _______ ___    _________
; Dept of Electrical &               / / / / ___//   |  / ____/   |
; Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
; 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
; USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
;
; ----------------------------------------------------------------------
;
;   FILENAME      : Lab 3
;   AUTHOR(S)     : Marc Rees
;   DATE             : 11/7/2018
;   COURSE       : ECE 382
;
;   Lab 2
;
;   DESCRIPTION   : This code simply provides a template for all
;                   C assignments to use.
;                     - Be sure to include your *Documentation*
;                       Statement below!
;
;   DOCUMENTATION : Cadet Martin helped me by explaining how to set up
;                                       the pulse duration for the irPacket and how to read it in
;                                       by shifting the irPacket by 2 by multiplying and adding 1 if it is a 1
;                                       and just multiplying if it was a 0. Also, Cadet Oliver helped me by
;                                       making sure my hex code was correct for my buttons for my LEDs to operate.
;
; Academic Integrity Statement: I certify that, while others may have
; assisted me in brain storming, debugging and validating this program,
; the program itself is my own work. I understand that submitting code
; which is the work of other individuals is a violation of the honor
; code.  I also understand that if I knowingly give my original work to
; another individual is also a violation of the honor code.
;
;**********************************************************************
//-----------------------------------------------------------------
#include <msp430g2553.h>
#include <stdint.h>
#include <stdbool.h>

// delay code written in assembly
// make sure to drag/drop the assembly code too
extern void Delay160ms(void);
extern void Delay40ms(void);

// macros to make life easier
#define        IR_PIN            (P2IN & BIT6)
#define        HIGH_2_LOW        P2IES |= BIT6
#define        LOW_2_HIGH        P2IES &= ~BIT6


// Globals are bad, but these are constants and can't be changed accidently.
// Set them up for your remote.
const uint32_t Button1 = 0x0D209F49;
const uint32_t Button2 = 0x0D10AF49;
const uint32_t Button3 = 0x0D308F49;
const uint32_t Button4 = 0x0D08B749;
const uint32_t Button5 = 0x0D289749;

uint32_t irPacket = 0;
uint32_t  irIndex = 0;

// -----------------------------------------------------------------------
// In order to decode IR packets, the MSP430 needs to be configured to
// tell time and generate interrupts on positive going edges.  The
// edge sensitivity is used to detect the first incoming IR packet.
// The P2.6 pin change ISR will then toggle the edge sensitivity of
// the interrupt in order to measure the times of the high and low
// pulses arriving from the IR decoder.
//
// The timer must be enabled so that we can tell how long the pulses
// last.  In some degenerate cases, we will need to generate a interrupt
// when the timer rolls over.  This will indicate the end of a packet
// and will be used to alert main that we have a new packet.
// -----------------------------------------------------------------------
void main(void){

    WDTCTL=WDTPW+WDTHOLD;  // stop WDT

    BCSCTL1 = CALBC1_8MHZ;
    DCOCTL = CALDCO_8MHZ;

    P2SEL  &= ~BIT6;    // Set up P2.6 as GPIO not XIN
    P2SEL2 &= ~BIT6;    // as before
    P2DIR &= ~BIT6;
    P2OUT |= BIT6;
    P2REN |= BIT6;
    P2IES |= BIT6;

    P2IFG &= ~BIT6;     // Clear any interrupt flag on P2.3
    P2IE  |= BIT6;      // Enable P2.3 interrupt

    HIGH_2_LOW;         // check the header out.  P2IES changed.

    P1DIR |= BIT0|BIT6; // Set LEDs as outputs
    P1DIR |= BIT4|BIT5;
    P1OUT &= ~(BIT0|BIT6); // And turn the LEDs off
    P1OUT &= ~(BIT4|BIT5);
                                    // ... you will need several lines to

    TA0CCR0 = 65000;    // create a 16ms roll-over period
    TACTL &= ~TAIFG;      // clear flag before enabling interrupts = good practice
    TA0CTL |= TASSEL_2 | MC_1 | ID_3 | TAIE;  // Use 1:8 prescalar off SMCLK and enable interrupts


    _delay_cycles(1000000); // toggle the Green led for on for 1 second
    P1OUT ^= BIT6;

    _enable_interrupt(); // and enable interrupts

    while(1){

                switch(irPacket){                         //take appropriate action
                   case Button1:
                       _delay_cycles(1000000);              // LED1 Turns on
                       P1OUT |= BIT4;
                       irPacket = 0;
                       break;

                   case Button2:
                       _delay_cycles(1000000);          // LED1 Turns Off
                       P1OUT &= ~BIT4;
                       irPacket = 0;
                       break;

                   case Button3:
                       P1OUT ^= BIT5;                       //LED2 Blinks 3 times
                       _delay_cycles(1000000);
                       P1OUT ^= BIT5;
                       _delay_cycles(1000000);
                       P1OUT ^= BIT5;
                       _delay_cycles(1000000);
                       P1OUT ^= BIT5;
                       _delay_cycles(1000000);
                       P1OUT ^= BIT5;
                       _delay_cycles(1000000);
                       P1OUT ^= BIT5;
                       irPacket = 0;
                       break;

                   case Button4:                                    //LED2 Blinks 5 Times
                       P1OUT ^= BIT5;
                       _delay_cycles(1000000);
                       P1OUT ^= BIT5;
                       _delay_cycles(1000000);
                       P1OUT ^= BIT5;
                       _delay_cycles(1000000);
                       P1OUT ^= BIT5;
                       _delay_cycles(1000000);
                       P1OUT ^= BIT5;
                       _delay_cycles(1000000);
                       P1OUT ^= BIT5;
                       _delay_cycles(1000000);
                       P1OUT ^= BIT5;
                       _delay_cycles(1000000);
                       P1OUT ^= BIT5;
                       irPacket = 0;
                       break;

                   case Button5:                                    //Led 1 and 2 turns on and off
                       _delay_cycles(1000000);
                       P1OUT ^= (BIT4|BIT5);
                       _delay_cycles(1000000);
                       P1OUT ^= (BIT4|BIT5);
                       irPacket = 0;
                       break;

                   irIndex = 0;
                   P1OUT &= ~BIT0;
                }



                        // however you are supposed to

    } // end infinite loop
} // end main



// -----------------------------------------------------------------------
// Since the IR decoder is connected to P2.6, we want an interrupt
// to occur every time that the pin changes - this will occur on
// a positive edge and a negative edge.
//
// Negative Edge:
// The negative edge is associated with end of the logic 1 half-bit and
// the start of the logic 0 half of the bit.  The timer contains the
// duration of the logic 1 pulse, so we'll pull that out, process it
// and store the bit in the global irPacket variable. Going forward there
// is really nothing interesting that happens in this period, because all
// the logic 0 half-bits have the same period.  So we will turn off
// the timer interrupts and wait for the next (positive) edge on P2.6
//
// Positive Edge:
// The positive edge is associated with the end of the logic 0 half-bit
// and the start of the logic 1 half-bit.  There is nothing to do in
// terms of the logic 0 half bit because it does not encode any useful
// information.  On the other hand, we going into the logic 1 half of the bit
// and the portion which determines the bit value, the start of the
// packet, or if the timer rolls over, the end of the ir packet.
// Since the duration of this half-bit determines the outcome
// we will turn on the timer and its associated interrupt.
// -----------------------------------------------------------------------
#pragma vector = PORT2_VECTOR
__interrupt void pinChange (void) {
    uint8_t    pin;
    uint16_t   pulseDuration;          // The timer is 16-bits


    if (IR_PIN)  // is pin high or low?
        pin=1;
    else
        pin=0;

    switch (pin) {
        case 0: // !!!!!!!!!NEGATIVE EDGE!!!!!!!!!!
            pulseDuration = TA0R;
            P1OUT |= BIT0;                                                                                          // Turns Red LED on
            if(irIndex >1){
                if(pulseDuration >1500 && pulseDuration < 3500)                     // If pulseDuration is in this window it places BIT into irPacket
                                 irPacket = (irPacket*2) + 1;
                            else
                                  irPacket = (irPacket *2);
            }


            TA0CTL &= ~MC_1;
            TA0CTL &= ~TAIE;

            LOW_2_HIGH;             // Set up pin interrupt on positive edge
            break;

        case 1: // !!!!!!!!POSITIVE EDGE!!!!!!!!!!!
            TA0R = 0x0000;          // time measurements are based at time 0
            TA0CTL |= MC_1 | TAIE;
            P1OUT &= ~BIT0;             //Turns RED LED off
            HIGH_2_LOW;             // Set up pin interrupt on falling edge
            break;
    } // end switch

    irIndex++;

    P2IFG &= ~BIT6;   // Clear the interrupt flag to prevent
                                      // immediate ISR re-entry

} // end pinChange ISR


#pragma vector = TIMER0_A1_VECTOR
__interrupt void timerOverflow (void) {
    TA0CTL &= ~MC_1;              // Turn off Timer A and Enable Interrupt
    TA0CTL &= ~TAIE;
    TA0CTL &= ~TAIFG;  // clear timer a interrupt
}
