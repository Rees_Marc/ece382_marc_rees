/*
 * ADC.h
 *
 *  Created on: Dec 3, 2018
 *      Author: C20Marc.Rees
 */

#ifndef ADC_H_
#define ADC_H_


// ADC10 interrupt service routine
#pragma vector=ADC10_VECTOR
__interrupt void ADC10_ISR(void)
{
  __bic_SR_register_on_exit(CPUOFF);        // Clear CPUOFF bit from 0(SR)
}

#pragma vector = TIMER1_A0_VECTOR
__interrupt void captureCompareInt (void) {
    P1OUT |= BIT0;                        //Turn on LED
    TA1CCTL1 &= ~CCIFG;                //clear capture compare interrupt flag
}

#pragma vector = TIMER1_A1_VECTOR
__interrupt void captureCompareInt2 (void) {
    P1OUT &= ~BIT0;                        //Turn off LED
    TA1CCTL1 &= ~CCIFG;                //clear capture compare interrupt flag


#endif /* ADC_H_ */
