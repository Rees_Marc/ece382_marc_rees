/*
 * LCD.h
 *
 *  Created on: Dec 3, 2018
 *      Author: C20Marc.Rees
 */

#ifndef LCD_H_
#define LCD_H_

void uart_putc(const int8_t c){
    while (!(IFG2 & UCA0TXIFG));  // USCI_A0 TX buffer ready?
    UCA0TXBUF = c;            // TX
    }



#endif /* LCD_H_ */
