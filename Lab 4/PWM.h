/*
 * PWM.h
 *
 *  Created on: Dec 3, 2018
 *      Author: C20Marc.Rees
 */

#ifndef PWM_H_
#define PWM_H_


void itoa(int16_t value, char* result, uint16_t base){
      // check that the base if valid
      if (base < 2 || base > 36) { *result = '\0';}

      char* ptr = result, *ptr1 = result, tmp_char;
      int16_t tmp_value;

      do {
        tmp_value = value;
        value /= base;
        *ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
      } while ( value );

      *ptr-- = '\0';
      while(ptr1 < ptr) {
        tmp_char = *ptr;
        *ptr--= *ptr1;
        *ptr1++ = tmp_char;
      }
}


}


#endif /* PWM_H_ */
