;**********************************************************************
;
; COPYRIGHT 2016 United States Air Force Academy All rights reserved.
;
; United States Air Force Academy     __  _______ ___    _________
; Dept of Electrical &               / / / / ___//   |  / ____/   |
; Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
; 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
; USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
;
; ----------------------------------------------------------------------
;
;   FILENAME      : Lab 4
;   AUTHOR(S)     : Marc Rees
;   DATE             : 12/3/2018
;   COURSE       : ECE 382
;
;   Lab 4
;
;   DESCRIPTION   : This code simply provides a template for all
;                   C assignments to use.
;                     - Be sure to include your *Documentation*
;                       Statement below!
;
;   DOCUMENTATION : Cadet Oliver helped me by explaining how the ADC worked and how to read
;                                  it and measure the PWM.
;
; Academic Integrity Statement: I certify that, while others may have
; assisted me in brain storming, debugging and validating this program,
; the program itself is my own work. I understand that submitting code
; which is the work of other individuals is a violation of the honor
; code.  I also understand that if I knowingly give my original work to
; another individual is also a violation of the honor code.
;
;**********************************************************************
//-----------------------------------------------------------------

#include <msp430.h> 
#include <stdint.h>
#include <stdbool.h>
#include 'LCD.h'
#include 'ADC.h'
#include 'PWM.h'

void uart_putc(const int8_t c);
void itoa(int16_t value, char* result, uint16_t base);

/**
 * main.c
 */
void main(void)
{
	WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer

    DCOCTL = 0;
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;

    P1SEL |= BIT2;
    P1SEL2 |= BIT2;

    UCA0CTL1 |= UCSSEL_2;  // select BRCLK = SMCLK
    UCA0CTL0 = 0;                 // 8N1
    UCA0BR0 = 104;               // p 424 1MHz@9600
    UCA0BR1 = 0;                   // p 424 1MHz@9600
    UCA0MCTL = UCBRS0;      // UCBRSx=1
    UCA0CTL1 &= ~UCSWRST;

    ADC10CTL0 = ADC10SHT_3 + ADC10ON + ADC10IE; // ADC10ON, interrupt enabled
    ADC10CTL1 = INCH_4;                       // input A4
    ADC10AE0 |= BIT4;                         // P1.4 ADC Analog enable
    ADC10CTL1 |= ADC10SSEL1|ADC10SSEL0;       // Select SMCLK
    P1DIR |= BIT0;                            // Set P1.0 to output direction

	ADC10CTL0 = ADC10SHT_3 + ADC10ON + ADC10IE; // ADC10ON, interrupt enabled
	ADC10CTL1 = INCH_4;                       // input A4
	ADC10AE0 |= BIT4;                         // P1.4 ADC Analog enable
	ADC10CTL1 |= ADC10SSEL1|ADC10SSEL0;       // Select SMCLK
	P1DIR |= BIT0;                            // Set P1.0 to output direction

	P2DIR |= BIT1;
	P2SEL |= BIT1;

	TA1CTL |= TASSEL_2|MC_1|ID_0;
	P1DIR  |= BIT0;
	P1REN |=BIT3;
	P1OUT |= BIT3;

	TA1CCR0 = 20000;
	TA1CCTL0 = CCIE;
	TA1CCTL0 &= ~CCIFG;

	TA1CCR1 = 650;
	TA1CCTL1 |= OUTMOD_7|CCIE;
	TA1CCTL1 &= ~CCIFG;

	uint16_t duty_cycles[] = {2850, 1650, 650};  //Sets up PWM cycles

	uart_putc(0xFE);            //Clears screen
	uart_putc(0x01);

	uart_putc('E');         //Sets up First line
	uart_putc('C');
	uart_putc('E');
	uart_putc(' ');
	uart_putc('3');
	uart_putc('8');
	uart_putc('2');
	uart_putc(',');
	uart_putc(' ');
	uart_putc('F');
	uart_putc('a');
	uart_putc('l');
	uart_putc('l');
	uart_putc(' ');
	uart_putc('2');
	uart_putc('0');
	uart_putc('1');
	uart_putc('8');
	uart_putc(' ');
	uart_putc(' ');

	uart_putc('C');           //Sets up second line
	uart_putc('2');
	uart_putc('C');
	uart_putc(' ');
	uart_putc('M');
	uart_putc('a');
	uart_putc('r');
	uart_putc('c');
	uart_putc(' ');
	uart_putc('R');
	uart_putc('e');
	uart_putc('e');
	uart_putc('s');

	__delay_cycles(3000000);        //Delays 3 seconds

	uart_putc(0xFE);            //Clears screen
	uart_putc(0x01);


	while(1){
	    ADC10CTL0 |= ENC + ADC10SC;             // Sampling and conversion start
	        __bis_SR_register(CPUOFF + GIE);        // LPM0, ADC10_ISR will force exit

	        uint16_t adc = ADC10MEM * 0.189;        //Conversation of adc to correct value

	        TA1CCR1 = 650 + (adc * 12.3);               //Conversation to read ADC to LCD

	        char new_result[3];                         // Creates new array to send angle to LCD

	        uart_putc(0xFE);                            //Clears LCD
	        uart_putc(0x01);

	        itoa(adc, new_result, 10);
	        uart_putc('S');
	        uart_putc('e');
	        uart_putc('r');
	        uart_putc('v');
	        uart_putc('o');
	        uart_putc(':');
	        uart_putc(new_result[0]);               //Prints angle numbers
	        uart_putc(new_result[1]);
	        uart_putc(new_result[2]);

	        __delay_cycles(1000000);

	}

}





